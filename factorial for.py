'''

recreate the factorial program using a for loop

'''

factorial = 1
num = int(input ("Enter a number: "))


for i in range (1, num + 1):
    factorial *= i
    
print (factorial)
