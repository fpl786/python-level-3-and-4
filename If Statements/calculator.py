'''

build a simple calculator program.

Ask user to enter first number

Ask the user to enter an operation (+, -, *, /)

Ask the user to enter second number

print the result


'''


num1 = int(input("Enter the first number: "))

operation = input("Enter the operation: ")

num2 = int(input("Enter the second number: "))

result = 0

error = False

if operation == "+":
    result = num1+num2
elif operation == "-":
    result = num1-num2
elif operation == "*":
    result = num1*num2
elif operation == "/":
    result = num1/num2
else:
    error = True
    print("Invalid Operation")
    
if error == False:
    print("Result: ", result)
