'''

Problem 1:

Take an integer as input.
print whether the number is positive or negative

HINT: Positive number is greater than 0
and a negative number is less than 0


Problem 2:
Take an integer as input.
Print whether the number is even or odd
HINT: Use the % operator

'''

number = int(input("Enter a number"))


if number > 0:
    print("positive")
elif number < 0:
    print("negative")
else:
    print("zero")



if number % 2 == 0:
    print("number is even")
else:
    print("number is odd")
