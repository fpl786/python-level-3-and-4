'''

Build a simple guessing game
generate a random number from 1 to 20
Ask the user to guess the number.

If the guess is correct, print "You Win"
otherwise print "You lose, the correct answer is ___"


Challenge:
Modify the program to allow the user to guess 3 times.
If any of the guesses are correct, the program should end
You cannot use any loops!
Hint: Nested if statements

'''
import random

number = random.randint(1, 20)

print(number)

guess = int(input("Guess the number: "))

if guess != number:
    print("Try Again")
    guess = int(input("Guess the number: "))
    if guess != number:
        print("Try Again")
        guess = int(input("Guess the number: "))
        if guess != number:
            print("You Lose, the correct answer is", number)
        else:
            print("You Win!")
        
    else:
        print("You Win!")
    
else:
    print("You Win!")
          
    
    
