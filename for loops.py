


#range(10) -> 0,1,2,3,4,5,6,7,8,9

for i in range(10):
    print(i)

#"hello" -> h,e,l,l,o

for i in "hello":
    print(i)


Sum = 0
for i in range(1, 1001):
    Sum += i

print(Sum)
