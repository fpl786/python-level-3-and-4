'''
Ask the user to enter a number
print a pyramid like below with that
number of rows

Hint: You need a while loop
      Use the string replication (*) operator for strings

Eg. 1

Enter a number: 5

*
**
***
****
*****

Eg. 2

Enter a number: 7

*
**
***
****
*****
******
*******

'''

counter = 1
number = int(input("Enter a number: "))


while counter <= number:
    print("*"*counter)
    counter += 1
