'''

write a guessing game using while loops.
Generate a random number from 1 to 1000.

Ask the user to guess a maximum of 10 times.

After each guess, if the guess is to high print "too high"
if the guess is too low, print "too low".

If the guess is correct, print "You win" and stop the program.


'''

import random

count = 0

number = random.randint(1, 1000)

while count < 10:
    guess = int(input("Guess the number: "))
    if guess > number:
        print("Too High")
    elif guess < number:
        print("Too Low!")
    else:
        print("You win!")
        break
    count += 1


if guess != number:
    print("You Lose! The correct number is", number)
